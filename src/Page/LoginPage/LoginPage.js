import React from "react";
import { Button, Checkbox, Form, Input, message } from "antd";
import { userServ } from "../../services/userServices";
import { useNavigate } from "react-router-dom";

export default function LoginPage() {
  let navigate = useNavigate();
  const onFinish = (values) => {
    userServ
      .postLogin(values)
      .then((res) => {
        console.log(res);
        message.success("Đăng nhập thành công");
        navigate("/");
        // đưa user về home page
      })
      .catch((err) => {
        message.error("Đăng nhập thất bại");

        console.log(err);
      });
  };
  //   abc123 123456
  //   message antd
  const onFinishFailed = (errorInfo) => {
    console.log("Failed:", errorInfo);
  };
  return (
    <div className="w-screen h-screen bg-red-600 flex items-center justify-center">
      <div className=" rounded p-10 bg-white w-11/12 ">
        <Form
          name="basic"
          labelCol={{
            span: 8,
          }}
          wrapperCol={{
            span: 16,
          }}
          style={{
            maxWidth: 600,
          }}
          initialValues={{
            remember: true,
          }}
          onFinish={onFinish}
          onFinishFailed={onFinishFailed}
          autoComplete="off"
        >
          <Form.Item
            label="Username"
            name="taiKhoan"
            rules={[
              {
                required: true,
                message: "Please input your username!",
              },
            ]}
          >
            <Input />
          </Form.Item>

          <Form.Item
            label="Password"
            name="matKhau"
            rules={[
              {
                required: true,
                message: "Please input your password!",
              },
            ]}
          >
            <Input.Password />
          </Form.Item>

          <Form.Item
            wrapperCol={{
              offset: 8,
              span: 16,
            }}
          >
            <Button type="primary" htmlType="submit" danger>
              Submit
            </Button>
          </Form.Item>
        </Form>
      </div>
    </div>
  );
}
// tailwind react installation
