import React from "react";
import ListMovie from "./ListMovie/ListMovie";

export default function HomePage() {
  return (
    <div>
      <ListMovie />
    </div>
  );
}
