import React, { useEffect } from "react";
import { movieServ } from "../../../services/movieServices";

export default function ListMovie() {
  useEffect(() => {
    movieServ
      .getMovieList()
      .then((res) => {
        console.log(res.data.content);
      })
      .catch((err) => {
        console.log(err);
      });
  }, []);
  return <div>ListMovie</div>;
}
