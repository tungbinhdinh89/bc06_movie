import axios from "axios";
import { BASE_URL, configHeaders } from "./ config";

export const movieServ = {
  getMovieList: () => {
    return axios.get(`${BASE_URL}/api/QuanLyPhim/LayDanhSachPhim?maNhom=GP07`, {
      headers: configHeaders(),
    });
  },
};
